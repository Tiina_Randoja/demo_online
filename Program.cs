﻿using System;

namespace demo_online
{
    class Program
    {
        static void Main(string[] args)
        { //Start the program with Clear();
        Console.Clear();
        
        
        //End the program with blank line and instructions
        Console.ResetColor();
        Console.WriteLine();
           Console.WriteLine("Hello World!");
        Console.WriteLine("Press <Enter> to quit the program");
        Console.ReadKey();
         
        }
    }
}
